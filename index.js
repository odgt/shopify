const {init:initializer, environment, console_color: colors} = require('./Server/config');
const broker = require('lib_broker_js');
const logger = require('lib_logger');

const __logger = logger.init(environment.logs);


const express = require('express');
const routes = require('./Server/Router/index')

///Inicializa la base de datos
const db = require('./Server/Controllers/Database')

const app = express();

initializer(app).then(async (app)=>{
    routes.mount(app)
    broker.init(environment.broker,__logger,routes.eventCallback);
   
    app.listen(environment.port,()=>{
        __logger.info(`[SR]  Shopify Service escuchando en puerto ${environment.port}`);
    })
})