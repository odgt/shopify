
const config = require('../config')
const subPath = config.environment.path+ "/Products";
const controller = require('../Controllers/products.controller');

module.exports = {
    mount: function(app){   

        app.post(subPath,async (req,res)=>{ 
            let response = await controller.getProducts(req.headers['x-consumer-custom-id'], req.body)
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })
    }
}