
const config = require('../config')
const subPath = config.environment.path + "/auth";
const controller = require('../Controllers/auth.controller');

module.exports = {
    mount: function(app){  

        // app.get(subPath,async (req,res)=>{
        //     //req.query.code es el auth token de microsoft
        //     //req.headers['x-consumer-custom-id'] es nuestro uuid de usuario que está haciendo el proceso de conexión

        //     /*
        //     A partir de aqui hay que obtener el access token y el refresh token canjeando el auth_token con el Client_ID de la
        //     aplicación. Si el auth aun es válido y los permisos del auth coinciden con lo esperado, microsoft debería darnos
        //     los dos token necesarios para mantener las comunicaciones siempre vivas
        //     */
        //     console.log(req.query)
        //     res.send(await controller.get(req.headers['x-consumer-custom-id'], req.query.code))
        // })

        app.delete(subPath,async (req,res)=>{           
            res.send(await controller.unconnect(req.headers['x-consumer-custom-id']))
        })
        //Actualiza el resource de dynamics
        app.put(subPath,async (req,res)=>{           
            res.send(await controller.update_token(req.headers['x-consumer-custom-id'], req.body.token))
        })
    }
}