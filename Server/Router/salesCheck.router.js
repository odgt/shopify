const config = require('../config')
const subPath = config.environment.path + "/SalesCheck";
const controller = require('../Controllers/salesCheck.controller');



module.exports = {
    mount: function(app){  

        app.post(subPath,async (req,res)=>{ 
            let response = await controller.CreateEmail(req.headers['x-consumer-custom-id'], req.body)
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })
        app.put(subPath,async (req,res)=>{ 
            let response = await controller.SendEmail(req.headers['x-consumer-custom-id'], req.body)
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })
    }
}