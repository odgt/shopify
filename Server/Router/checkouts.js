
const config = require('../config')
const subPath = config.environment.path+ "/checkouts";
const controller = require('../Controllers/checkouts.controller');

module.exports = {
    mount: function(app){  

        app.get(subPath,async (req,res)=>{ 
            let response = await controller.getCheckouts(req.headers['x-consumer-custom-id'])
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })

    }
}