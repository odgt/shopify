const mfe = require('./mfe.router');
const user = require('./user.router');
const auth = require('./auth.router');
const connect = require('./connect.router');
const base = require('./base.router');
const contacts = require('./contacts.router');
const products = require('./products.router');
const orders = require('./orders.router');
const salesCheck = require('./salesCheck.router');
const config = require('../config');
module.exports = {
    mount:function(app){
        mfe.mount(app);
        auth.mount(app);
        connect.mount(app);
        base.mount(app);
        contacts.mount(app);
        products.mount(app);
        orders.mount(app);
        salesCheck.mount(app);

    },
    eventCallback: function(msg, processed){
       
        let topicDesglose = msg.fields.routingKey.split(".")

        switch(topicDesglose[0]){
            case "user":

                user.eventRecibed(msg,processed);
                break;
            default:
                processed();
        }
       
    }
}