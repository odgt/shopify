
const config = require('../config')
const subPath = config.environment.path+ "/contacts";
const controller = require('../Controllers/contacts.controller');

module.exports = {
    mount: function(app){  

        app.get(subPath + "/:userEmail",async (req,res)=>{ 
            let response = await controller.getContacts(req.headers['x-consumer-custom-id'],req.params.userEmail)
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })

        app.post(subPath,async (req,res)=>{ 
            let response = await controller.CreateContact(req.headers['x-consumer-custom-id'], req.body)
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })
    }
}