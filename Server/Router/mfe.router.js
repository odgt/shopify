
const config = require('../config')
const subPath = config.environment.path;
const controller = require('../Controllers/front.controller');
const path = require('path')
module.exports = {
    mount: function(app){  
        ///Retorna el MFE para realizar la conexión con dynamics
        app.get(subPath + "/connect.html",async (req,res)=>{
            res.sendFile(path.join(__dirname, "../MFE/connect.html"))
            //res.send(await controller.get(req.headers['x-consumer-custom-id']))
        })
    }
}