
const config = require('../config')
const subPath = config.environment.path + "/connect";
const controller = require('../Controllers/connect.controller');

module.exports = {
    mount: function(app){  

        app.get(subPath + "/status",async (req,res)=>{           
            res.send(await controller.getStatus(req.headers['x-consumer-custom-id']))
        })
        
    }
}