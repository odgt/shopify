const config = require('../config')
const subPath = config.environment.path + "/orders";
const controller = require('../Controllers/orders.controller');



module.exports = {
    mount: function(app){  

        app.post(subPath,async (req,res)=>{ 
            let response = await controller.CreateOrder(req.headers['x-consumer-custom-id'], req.body)
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })
        app.put(subPath,async (req,res)=>{ 
            let response = await controller.UpdateOrder(req.headers['x-consumer-custom-id'], req.body)
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })
    }
}