
const config = require('../config')
const subPath = config.environment.path;
const controller = require('../Controllers/user.controller');

module.exports = {
    eventRecibed: function(msg, processed){
        if(msg.fields.routingKey.match(/user.*.created/) !== null){         
            controller.createdEvent(msg, processed)
        }
        if(msg.fields.routingKey.match(/user.*.updated/) !== null){ 
            controller.updatedEvent(msg, processed)
        }
        if(msg.fields.routingKey.match(/user.*.deleted/) !== null){
            controller.deletedEvent(msg, processed)
        }
       
    }
}