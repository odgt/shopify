
const config = require('../config')
const subPath = config.environment.path + "/api";
const controller = require('../Controllers/base.controller');

module.exports = {
    mount: function(app){  

        app.get(subPath + "/demo",async (req,res)=>{ 
            let response = await controller.demo(req.headers['x-consumer-custom-id'])
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })
       

        app.get(subPath + "/miendpoint",async (req,res)=>{ 
            let response = {data:{hola: "hola"}}
            if(!response.error){
                res.send(response.data)
            }else{
                res.sendStatus(response.error.status)
            }
        })
    }
}