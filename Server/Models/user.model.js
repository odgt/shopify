const { Sequelize, DataTypes} = require('sequelize');
const crypto = require('crypto');
const config = require('../config');

module.exports = function(sequelize){
    return sequelize.define('Users',{
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
        },
        name:{
            type: DataTypes.STRING,
            allowNull: false,
        },
        token:{
            type: DataTypes.STRING(3000),
            allowNull: true
        }
    }, {})
}