const express = require('express');
const cookieParser = require('cookie-parser');

module.exports = {
    init: function(app){
        return new Promise((accept,canc) => {
            try{
                //app.use("/static",express.static('public'))
                app.use(express.json());
                app.use(express.urlencoded({extended:true}))
                app.use(cookieParser())
                accept(app)
            }catch(e){
                canc(e)
            }
            
        })
        
    },
    environment:{
        path: process.env.ADMIN_MICROSERVICE_BASE_PATH || "/shopify",//el path base del que partirán las peticiones de la api
        port: (process.env.ADMIN_PORT?parseInt(process.env.ADMIN_PORT):3000),//Puerto por el que escucha la app
        db:{//Configuración de base de datos usada en la aplicación
            db_host: process.env.ADMIN_DB_HOST||"shopify-database",//Host de DB
            db_name: process.env.ADMIN_DB_NAME||"shopify_schema",//Nombre de la DB Usada
            db_user: process.env.ADMIN_DB_USER||"root",//Usuario usado para trabajar en la DB
            db_pass: process.env.ADMIN_DB_PASS||"root",//Contraseña del usuario usado para trabajar en DB
            db_dialect: process.env.ADMIN_DB_DIALECT||"mysql",//Cambia el motor de db sobre el que actual el ORM, necesita de mas configuración que de momento no es dinámica
            sync: {
                schema: (process.env.ADMIN_DB_SYNC_SCHEMA?process.env.ADMIN_DB_SYNC_SCHEMA==="true":false),//Al iniciar intenta sincronizar modelos y tablas sin eliminar base de datos
                data: (process.env.ADMIN_DB_SYNC_DATA?process.env.ADMIN_DB_SYNC_DATA==="true":false)//Al iniciar intenta sincronizar datos con los dueños canónicos
            }
        },
        broker:{
            dns: process.env.ADMIN_BROKER_HOST||"rabbitMQ",//DNS Interno del broker de mensajería
            global_exchange: process.env.ADMIN_BROKER_GLOBAL_EXCHANGE||"global",//Exchange donde apunta su cola
            listen_queue: process.env.ADMIN_BROKER_LISTEN_QUEUE||"shopify",//Cola local en la que escucha
            topics: ["user.#.created", "user.#.deleted"],//Topics a los que se subscribe el servicio
            listen: (process.env.ADMIN_BROKER_LISTEN_ACTIVE?process.env.ADMIN_BROKER_LISTEN_ACTIVE==="true":true),//ACtiva/desactiva la escucha de mensajes del broker
            user: process.env.ADMIN_BROKER_USER || "",//Usuario para conectarse al broker
            pass: process.env.ADMIN_BROKER_PASS || "",//Pass para conectarse al broker
            useCredentials: (process.env.ADMIN_BROKER_USE_CREDENTIALS?process.env.ADMIN_BROKER_USE_CREDENTIALS === "true":false),//Conectarse al broker usando credenciales o no
            connect: (process.env.ADMIN_BROKER_CONNECT? process.env.ADMIN_BROKER_CONNECT === "true":true),//Conecta o no con el broker
            secure: (process.env.ADMIN_BROKER_SECURE? process.env.ADMIN_BROKER_SECURE === "true":false)
        },
        sync:{
            providers:{
                users:{
                    host:process.env.ADMIN_SYNC_PROVIDERS_USERS_HOST||"app-admin",
                    port:(process.env.ADMIN_SYNC_PROVIDERS_USERS_PORT?parseInt(process.env.ADMIN_SYNC_PROVIDERS_USERS_PORT):3000),
                    endpoint: process.env.ADMIN_SYNC_PROVIDERS_USERS_ENDPOINT || "/owner/sync/user",
                    secret: process.env.ADMIN_SYNC_PROVIDERS_USERS_SECRET || "UWZZqfVfhjBffSfDH4Tx7hWe6Mjqe5hE"
                }
            }
        },
        shopify_app:{
            baseUrl: "https://inconcertcc.myshopify.com/admin/api/2022-01/graphql.json",
            headerKey: "X-Shopify-Access-Token"
        },
        logs:{
            host: process.env.ADMIN_LOGS_HOST || "logstash",
            port: (process.env.ADMIN_LOGS_PORT?parseInt(process.env.ADMIN_LOGS_PORT):3000),
            metadata:{
                service: "shopify"
            }
        }
    },
    console_color:{
        red: "\x1b[31m",
        green: "\x1b[32m",
        blue:"\x1b[34m",
        yellow: "\x1b[33m",
        default: "\x1b[0m"
    }
}
