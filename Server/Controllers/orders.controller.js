const api_controller = require('./api.controller')

module.exports = {
    CreateOrder: async function(uuid,bodyPost){
          const body ={
            query: `mutation {
                draftOrderCreate(input:{
                    customerId: "${bodyPost.idCliente}",
                    note: Venta por ChatBot, 
                    email: "${bodyPost.email}", 
                    taxExempt: false,
                    tags: [
                        "${bodyPost.tags}"
                    ],
                    shippingLine: {
                        title: "Valor envio",
                        price: "${bodyPost.precioEnvio}"
                    },
                    shippingAddress: {
                        address1: "${bodyPost.direccionEnvio}",
                        city: "${bodyPost.ciudadEnvio}",
                        province: "${bodyPost.regionEnvio}",
                        country: "${bodyPost.paisEnvio}"
                    },
                    billingAddress: {
                        address1: "${bodyPost.direccionEnvio}",
                        city: "${bodyPost.ciudadEnvio}",
                        province: "${bodyPost.regionEnvio}",
                        country: "${bodyPost.paisEnvio}"
                    },
                    appliedDiscount: {
                        description: "Descuento por pedido",
                        value: "${bodyPost.valorDescuento}",
                        valueType: PERCENTAGE,
                        title: "Custom"
                    },
                    lineItems: [
                        {
                            variantId: "${bodyPost.idVarianteProducto}",
                            quantity: "${bodyPost.cantidadProducto}"
                        },
                    ],
                    customAttributes: [
                    {
                        key: "Venta por Chatbot",
                        value: "Venta por Chatbot"
                    }
                    ]
                } ) {
                draftOrder {
                    id      
                    email
                    note2
                    invoiceSentAt
                    invoiceUrl
                    status
                    subtotalPrice
                    totalShippingPrice
                    totalPrice        
                    totalTax
                    customer
                    {
                        id
                        firstName
                        lastName
                    }
                    order
                    {
                        id
                        createdAt
                        note
                        lineItems (first: 100)
                        {
                            edges 
                            { 
                                node 
                                {
                                    currentQuantity
                                    quantity
                                    sku
                                    title
                                    id
                                    originalUnitPrice
                                    originalTotal
                                    variant
                                    {
                                        id
                                        title
                                    }
                                }
                            }
                        }
                    }        
                }
              }
           }`
           }
                return await api_controller.send(body,uuid);
    },
    UpdateOrder: async function(uuid,bodyPost){
        const body ={
          query: `mutation {
            draftOrderUpdate(id: "${bodyPost.draftOrderId}", input: {
                customerId: "${bodyPost.idCliente}",
                note: "${bodyPost.note}",
                email: "${bodyPost.email}",
                taxExempt: false,
                tags: [
                    "${bodyPost.tag1}",
                    "${bodyPost.tag2}"
                ],
                shippingLine: {
                    title: "Valor envio'",
                    price: ${bodyPost.precioEnvio}
                },
                shippingAddress: {
                    address1: "${bodyPost.direccionEnvio}",
                    city: "${bodyPost.ciudadEnvio}",
                    province: "${bodyPost.regionEnvio}",
                    country: "${bodyPost.paisEnvio}"
                },
                billingAddress: {
                    address1: "${bodyPost.direccionEnvio}",
                    city: "${bodyPost.ciudadEnvio}",
                    province: "${bodyPost.regionEnvio}",
                    country: "${bodyPost.paisEnvio}"
                },
                appliedDiscount: {
                    description: "Descuento por pedido",
                    value: "${bodyPost.valorDescuento}",
                    valueType: PERCENTAGE,
                    title: "Custom"
                },
                lineItems: [
                    {
                        variantId: "${bodyPost.idVarianteProducto}",
                        quantity: "${bodyPost.cantidadProducto}"
                    }
                ],
                customAttributes: [
                {
                    key: "test",
                    value: "Prueba atributo 2"
                }]
            } ) {
            draftOrder {
                id      
                email
                note2
                invoiceSentAt
                invoiceUrl
                status
                subtotalPrice
                totalShippingPrice
                totalPrice        
                totalTax
                customer
                {
                    id
                    firstName
                    lastName
                }
                order
                {
                    id
                    createdAt
                    note
                    lineItems (first: 100)
                    {
                        edges 
                        { 
                            node 
                            {
                                currentQuantity
                                quantity
                                sku
                                title
                                id
                                originalUnitPrice
                                originalTotal
                                variant
                                {
                                    id
                                    title
                                }
                            }
                        }
                    }
                }        
            }
          }
        }`
         }
              return await api_controller.send(body,uuid);
        }
   }