const axios = require('axios')
const config = require('../config');
const db = require('./Database');

const baseUsers = `http://${config.environment.sync.providers.users.host}:${config.environment.sync.providers.users.port}`



module.exports = {
    user:async function(){
        let usersResponse = await axios.get(`${baseUsers}${config.environment.sync.providers.users.endpoint}`,{
            headers:{
                'provider_key':config.environment.sync.providers.users.secret
            }
        })
        let users = usersResponse.data;
        
        let t = await db.sequelize.transaction();
        try {
            await db.User.truncate({transaction:t});
            await db.User.bulkCreate(users,{transaction:t})
            t.commit();
        } catch (error) {
            t.rollback();
            console.log(error)
        }
        
        return null;
    }
}