const api_controller = require('./api.controller')

module.exports = {
    getCheckouts: async function(uuid,params){
        return await api_controller.send(params,uuid);
    }
}