const db = require('./Database');
const crypto = require('crypto');
const config = require('../config');

const {logger} = require("lib_logger");

const {Op} = require('sequelize')

module.exports = {
    createdEvent:async function(msg, processed){
        let user = JSON.parse(Buffer.from(msg.content).toString('utf-8')).user
        
        //console.log(user)
        let t = await db.sequelize.transaction();
        try {
            await db.User.create(user,{transaction:t})
            processed();
            t.commit()
            logger.info("User created", {user});
        } catch (error) {
            t.rollback();
            logger.error("Error creating user",{user,error})
            throw(error)
        }
       
    },
    updatedEvent:async function(msg, processed){
        let user = JSON.parse(Buffer.from(msg.content).toString('utf-8')).user
        let t = await db.sequelize.transaction();
        try{
            await db.User.update(user,{
                where:{
                    id:user.id
                },transaction:t
            })
            processed();
            t.commit();
            logger.info("User updated", {user});
        }catch(e){
            t.rollback();
            logger.error("Error updating user",{user,error:e});
            throw e;
        }
        
     },
     deletedEvent:async function(msg, processed){
        //console.log(JSON.parse(Buffer.from(msg.content).toString('utf-8')).user)
        let user = JSON.parse(Buffer.from(msg.content).toString('utf-8')).user
        let t = await db.sequelize.transaction();
        try{
            await db.User.destroy({
                where:{
                    id:user.id
                },transaction:t
            })
            processed();
            t.commit();
            logger.info("User deleted",{user})
        }catch(e){
            t.rollback();
            logger.error("Error deleting user",{user,error:e})
            throw e;
        }
        
     }
}