const api_controller = require('./api.controller')

module.exports = {
    getContacts: async function(uuid,userEmail){
        const body = {query:`{
                customers(first: 10, query:"email:${userEmail}") 
                {
                    edges {
                        node {
                            id
                            email
                            firstName
                            lastName
                            note
                            phone
                            state
                            taxExempt
                            addresses
                            {
                                address1
                                address2
                                city
                                company
                                country
                                formatted
                                formattedArea
                                latitude
                                longitude
                                phone
                            }
                        }
                    }
                }
        }`}
        return await api_controller.send(body,uuid);
    },
    CreateContact: async function(uuid,bodyPost){
          const body ={
            query: `mutation {
                customerCreate(input:{
                    email: "${bodyPost.email}",
                    phone: "${bodyPost.phone}", 
                    firstName: "${bodyPost.firstName}", 
                    lastName: "${bodyPost.lastName}"
                    }) { 
                        userErrors { 
                            field 
                            message 
                           } 
                   customer { 
                       id 
                       email 
                       phone 
                       firstName 
                       lastName 
                       verifiedEmail
                           }
                   }
           }`
           }
                return await api_controller.get(body,uuid);
    }
}