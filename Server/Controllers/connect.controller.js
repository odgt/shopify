const db = require('./Database');
const crypto = require('crypto');
const config = require('../config');
const atob = require("atob");

const {Op} = require('sequelize')

module.exports = {
    getStatus: async function(uuid){
        //Debemos devolver el estado de la conexión del usuario
        let user = await db.User.findOne({where:{id:uuid}})
        let status = {};
        //
        // DYNAMICS
        //
        // status.resource = user.dynamics_resource;
        // if(user.refresh_token == null && user.access_token == null){
        //     status.state = "disconnected";
        //     status.step = 0;
            
        //     status.auth_uri = config.environment.dynamics_app.base_url + "/authorize?client_id=" + config.environment.dynamics_app.client_id + "&prompt=consent&response_type=code&response_mode=query&scope=" + config.environment.dynamics_app.scope_auth.join(" ")
            
        // }else{
        //     //TODO, comprobación si las credenciales son válidas para llamar a microsoft
        //     if(false){//Si no se ha podido usar la api
        //         status.state = "disconnected";
        //         status.step = 1;
                
        //     }else{
        //         status.state = "connected";
        //         status.step = 2;
        //         //console.log(user.access_token.split(".")[1])
        //         let userInfo = JSON.parse(atob(user.access_token.split(".")[1]))
        //         //console.log(userInfo)
                
        //         status.connectedInfo = {
        //             name: userInfo.name,
        //             email: userInfo.unique_name
        //         }
        //     }
        // }

        if(user.token){
            status.state= "connected";
            status.step=2
            status.token = user.token;
        }else{
            status.state= "disconnected";
            status.step = 0;
        }

        return status
    }
}