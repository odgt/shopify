const api_controller = require('./api.controller')

module.exports = {
    demo: async function(uuid){
        let body = {"query":`{
            shop{
                currencyCode  
                      contactEmail
            }
            products(first: 10, query: "inventory_total:>1 AND title:coca cola AND status:DRAFT AND (tag:1500ml OR tag:2500ml OR tag:gaseosa) AND (productType:gaseosa OR productType:bebida)")
            { 
                edges 
                { 
                    node 
                    {
                        id
                        title 
                        description 
                        onlineStorePreviewUrl
                        onlineStoreUrl
                        publishedAt
                        status
                        totalInventory
                        tags
                        productType
                        variants(first: 3)
                        {
                            edges
                            {
                                node
                                {
                                    sku
                                    inventoryQuantity
                                    title
                                    price
                                }
                            }
                        }
                        standardizedProductType
                        {
                            productTaxonomyNode
                            {
                                fullName
                                id
                                name
                            }
                        }
                    }  
                }      
            }
        }`}
        return await api_controller.send(body,uuid);
    }
}