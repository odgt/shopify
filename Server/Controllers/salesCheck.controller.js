const api_controller = require('./api.controller')

module.exports = {
    CreateEmail: async function(uuid,bodyPost){
          const body ={
            query: `mutation {
                draftOrderInvoicePreview(id: "${bodyPost.draftOrderId}", email: {
                    #body: "${bodyPost.mensaje}"
                    customMessage: "${bodyPost.mesajeCustom}"
                    from: "${bodyPost.email}"
                    subject: "${bodyPost.asunto}"
                    to: "${bodyPost.emailDestinatario}"
                }) {
                    previewHtml
                    userErrors {
                        field
                        message
                    }
                }
            }`
           }
                return await api_controller.send(body,uuid);
    },
    SendEmail: async function(uuid,bodyPost){
        const body ={
          query: `mutation {
            draftOrderInvoiceSend(id: "${bodyPost.draftOrderId}", email: {
                #body: "${bodyPost.mensaje}"
                customMessage: "${bodyPost.mesajeCustom}"
                from: "${bodyPost.email}"
                subject: "${bodyPost.asunto}"
                to: "${bodyPost.emailDestinatario}"
            }) {
                draftOrder
                {
                    email
                    id
                    invoiceSentAt
                    invoiceUrl
                    completedAt
                    customer
                    {
                        firstName
                        lastName
                        id
                        phone
                    }
                }
                userErrors {
                    field
                    message
                }
            }
        }`
         }
              return await api_controller.send(body,uuid);
        }
   }