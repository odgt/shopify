const config = require('../config');
const mysql = require('mysql2/promise');
const {Sequelize} = require('sequelize');

module.exports = db = {};

initialize();

async function initialize(){
    const connection = await mysql.createConnection({
        host: config.environment.db.db_host,
        user: config.environment.db.db_user,
        password: config.environment.db.db_pass
    });

    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${config.environment.db.db_name}\`;`)

    const sequelize = new Sequelize(
        config.environment.db.db_name, 
        config.environment.db.db_user, 
        config.environment.db.db_pass,
        {
            host: config.environment.db.db_host,
            dialect: config.environment.db.db_dialect
        });

        db.sequelize = sequelize;
        //Exportamos al objeto db los modelos
        db.User = require('../Models/user.model')(sequelize);

        if(config.environment.db.sync.schema){
            await sequelize.sync({alter: true})
        }
        if(config.environment.db.sync.data){
            //PROCESO DE SINCRONIZACIÓN DE DATOS CON PROVEEDORES
            //En este caso, se depende de los usuarios que tenga admin-tools en su base de datos
            const sync = require('./Sync.controller');
            await sync.user();
        }
}