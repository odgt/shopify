const db = require('./Database');
const crypto = require('crypto');
const config = require('../config');
const axios = require("axios");
const auth_controller = require('./auth.controller')

module.exports = {
    __genericSend: async function(configuration){
        if(!configuration.intent){
            configuration.intent = 0;
        }else{
            configuration.intent += 1;
        }
        let {token} = await db.User.findOne({where:{id:configuration.uuid}})
      
        let error = undefined
        let response = undefined;
        try {      

            let headers = {}
            headers[config.environment.shopify_app.headerKey] = token          
            response = await axios.post(config.environment.shopify_app.baseUrl,configuration.body,{headers})
            return {error: null, data: response.data}
        } catch (e) {
            
            console.error(e.message)
            error = e
        }
        return {error: {status:500, message:"unexpected", error}};
    },
    send: async function(body, uuid){
        console.log(body);
        return await this.__genericSend({uuid:uuid, body:body})
    }
}