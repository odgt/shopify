const api_controller = require('./api.controller')

module.exports = {
    getProducts: async function(uuid,bodyPost){
        const body = {query:`{
            shop{
                currencyCode        
            }
            products(
                first: 10, 
                query: "inventory_total:>0 AND title:${bodyPost.articulo} AND status:${bodyPost.estado} AND (tag:${bodyPost.tag1} OR tag:${bodyPost.tag2} OR tag:${bodyPost.tipoArticulo1} OR tag:${bodyPost.tipoArticulo2}) AND (productType:${bodyPost.tipoArticulo1} OR productType:${bodyPost.tipoArticulo2})"
            )
            {
                edges 
                { 
                    node 
                    {
                        id
                        title 
                        description 
                        onlineStorePreviewUrl
                        onlineStoreUrl
                        publishedAt
                        status
                        totalInventory
                        tags
                        productType
                        variants(first: 10)
                        {
                            edges
                            {
                                node
                                {
                                    id
                                    sku
                                    inventoryQuantity
                                    title
                                    price
                                }
                            }
                        }
                        standardizedProductType
                        {
                            productTaxonomyNode
                            {
                                fullName
                                id
                                name
                            }
                        }
                    }  
                }      
            }
        }`}
        return await api_controller.send(body,uuid);
    }
}