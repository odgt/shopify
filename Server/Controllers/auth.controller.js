const db = require('./Database');
const crypto = require('crypto');
const config = require('../config');
const axios = require("axios");
const broker = require('lib_broker_js')
const atob = require('atob');

const {logger} = require('lib_logger');

const {Op} = require('sequelize')

module.exports = {
    /// OAUTH DYNAMICS, REDIRECT INPUT
    // get: async function(uuid, authCode){
    //     let user = await db.User.findOne({where:{id:uuid}})

    //     //Debemos canjear el authCode por un accesstoken y un refreshtoken valido
    //     let token_uri = config.environment.dynamics_app.base_url + "/token?"

    //     let params = new URLSearchParams();
    //     params.append("client_id",config.environment.dynamics_app.client_id);
    //     params.append("code",authCode)
    //     params.append("grant_type","authorization_code")
    //     params.append("redirect_uri",config.environment.dynamics_app.redirect_uri)

    //     let scope_access = []
    //     config.environment.dynamics_app.scope_token.forEach((scope)=>{
    //         scope_access.push(scope.replace("$resource", user.dynamics_resource))
    //     })
    //     params.append("scope",scope_access.join(" "))
    //     params.append("client_secret", encodeURI(config.environment.dynamics_app.auth_secret));
    //     let auth_response = undefined
    //     try{
    //         auth_response = await axios.post(token_uri, params);
    //         let user = {
    //             access_token:auth_response.data.access_token,
    //             refresh_token: auth_response.data.refresh_token,
    //             dynamics_resource: JSON.parse(atob(auth_response.data.access_token.split(".")[1])).aud
    //         }          
    //         let t = await db.sequelize.transaction();
    //         try{
    //             await db.User.update(user,{
    //                 where:{
    //                     id:uuid
    //                 },transaction:t
    //             })     
    //             broker.send(`dynamics.${uuid}.authenticated`, user)      
    //             t.commit();
    //         }catch(e){
    //             t.rollback();
    //             throw e;
    //         }
    //         //console.log(auth_response.data.access_token);
    //         //console.log(auth_response.data.refresh_token);
    //     }catch(e){
    //         console.log(e)
    //     }
        
        
    //     return "<script>window.location.href = 'https://app.backend-dev.inconcertcc.com/#/dynamics/connect'</script>"
    // },
    unconnect: async function(uuid){
        let t = await db.sequelize.transaction();
        let user = {
            token:null
        }
        try{
            await db.User.update(user,{
                where:{
                    id:uuid
                },transaction:t
            })     
            broker.send(`shopify.${uuid}.unauthenticated`, user)      
            t.commit();
            logger.info("User unconnected",{user:{id:uuid}});
        }catch(e){
            t.rollback();
            logger.error("Error unconnecting user",{user:{id:uuid}, error:e})
            throw e;
        }
        return true;
    },
    update_token: async function(uuid, token){
       
        let t = await db.sequelize.transaction();
        try{
            await db.User.update({token}, {where:{id:uuid}, transaction:t})   
            broker.send(`shopify.${uuid}.authenticated`, {id:uuid, token:token})      
            t.commit();
            logger.info("Token updated for user",{user:{id:uuid}});
        }catch(e){
            t.rollback();
            logger.error("Error updating token",{user:{id:uuid}, error:e})
            throw e;
        }
        
        return true
    }
    ///PROCESO REFRESH OAUTH DYNAMICS
    // ,
    // refresh: async function(uuid, refresh_token){
    //     let token_uri = config.environment.dynamics_app.base_url + "/token?"

    //     let params = new URLSearchParams();
    //     params.append("client_id",config.environment.dynamics_app.client_id);
    //     params.append("refresh_token",refresh_token)
    //     params.append("grant_type","refresh_token")
    //     params.append("client_secret", encodeURI(config.environment.dynamics_app.auth_secret));
    //     let auth_response = undefined
    //     try{
    //         auth_response = await axios.post(token_uri, params);
    //         let user = {
    //             access_token:auth_response.data.access_token,
    //             refresh_token: auth_response.data.refresh_token
    //         }          
    //         let t = await db.sequelize.transaction();
    //         try{
    //             await db.User.update(user,{
    //                 where:{
    //                     id:uuid
    //                 },transaction:t
    //             })     
    //             broker.send(`dynamics.${uuid}.refreshcredentials`, user)      
    //             t.commit();
    //             return user;
    //         }catch(e){
    //             t.rollback();
    //             throw e;
    //         }
    //         //console.log(auth_response.data.access_token);
    //         //console.log(auth_response.data.refresh_token);
    //     }catch(e){
    //         console.log(e)
    //     }
    //     return false;
    // }

}