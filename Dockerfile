FROM node:16.14.0
WORKDIR /app
RUN npm install --location=global npm@8.12.1

ENV NODE_ENV=production

COPY ["package.json","package-lock.json",".npmrc","./"]

RUN npm install
COPY . ./
CMD ["node","index.js"]